create table public.company
(
    id_company   bigint not null
        primary key,
    name_company varchar(255)
);

alter table public.company
    owner to postgres;

create table public.site
(
    id_site     bigint not null
        primary key,
    city        varchar(255),
    country     varchar(255),
    latitude    double precision,
    longitude   double precision,
    id_topology bigint
        constraint fks5to6hfq8bva54icntdqx8m6y
            references public.topology
);

alter table public.site
    owner to postgres;


create table public.topology
(
    id_topology   bigint not null
        primary key,
    name_topology varchar(255),
    id_company    bigint
        constraint uk_topology_id_company
            unique
        constraint fk96sywnfljg5ymm1j012r1140r
            references public.company
);

alter table public.topology
    owner to postgres;