package tn.sofrecom.servicelocalisation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServiceLocalisationApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServiceLocalisationApplication.class, args);
    }

}
