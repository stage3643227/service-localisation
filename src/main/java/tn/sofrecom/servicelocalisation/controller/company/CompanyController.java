package tn.sofrecom.servicelocalisation.controller.company;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tn.sofrecom.servicelocalisation.dto.APIResponse;
import tn.sofrecom.servicelocalisation.dto.company.CompanyRequestDTO;



public interface CompanyController {
    @PostMapping
    ResponseEntity<APIResponse> createNewCompany(CompanyRequestDTO companyRequestDTO);
    @PutMapping
    ResponseEntity<APIResponse> updateCompany(CompanyRequestDTO companyRequestDTO);
    @DeleteMapping("/{idCompany}")
    ResponseEntity<Void> deleteCompany(@PathVariable Long idCompany);
    @GetMapping("/{idCompany}")
    ResponseEntity<APIResponse> getCompanyById(@PathVariable Long idCompany);
    @GetMapping("/all")
    ResponseEntity<APIResponse> getAllCompanies();

}
