package tn.sofrecom.servicelocalisation.controller.company.implement;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tn.sofrecom.servicelocalisation.controller.company.CompanyController;
import tn.sofrecom.servicelocalisation.dto.APIResponse;
import tn.sofrecom.servicelocalisation.dto.company.CompanyRequestDTO;
import tn.sofrecom.servicelocalisation.dto.company.CompanyResponseDTO;
import tn.sofrecom.servicelocalisation.service.company.implement.CompanyServiceImp;

import javax.validation.Valid;
import java.util.Collection;
import java.util.List;

@RestController
@AllArgsConstructor
@Slf4j
@RequestMapping("/api/v1/Companies")
@CrossOrigin(origins = "http://localhost:4200")
public class CompanyControllerImp implements CompanyController {

    public static final String SUCCESS = "Success";
    private CompanyServiceImp companyServiceImp;
    @Override
    public ResponseEntity<APIResponse> createNewCompany(@Valid @RequestBody CompanyRequestDTO companyRequestDTO) {
        log.info("CompanyController::createNewCompany request body {}", companyRequestDTO);

        CompanyResponseDTO companyResponseDTO = companyServiceImp.createNewCompany(companyRequestDTO);
        APIResponse<CompanyResponseDTO> responseDTO = APIResponse
                .<CompanyResponseDTO>builder()
                .status(SUCCESS)
                .results(companyResponseDTO)
                .build();

        log.info("CompanyController::createNewCompany response {}", companyResponseDTO );
        return new ResponseEntity<>(responseDTO, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<APIResponse> updateCompany(@Valid @RequestBody CompanyRequestDTO companyRequestDTO) {
        log.info("CompanyController::updateCompany request body {}", companyRequestDTO);
        CompanyResponseDTO companyResponseDTO = companyServiceImp.updateCompany(companyRequestDTO);
        APIResponse<CompanyResponseDTO> responseDTO = APIResponse
                .<CompanyResponseDTO>builder()
                .status(SUCCESS)
                .results(companyResponseDTO)
                .build();

        log.info("CompanyController::updateCompany response {}", companyResponseDTO );
        return new ResponseEntity<>(responseDTO, HttpStatus.ACCEPTED);
    }

    @Override
    public ResponseEntity<Void> deleteCompany(Long idCompany) {
        log.info("CompanyController::deleteCompany by idCompany  {}", idCompany);
        companyServiceImp.deleteCompany(idCompany);
        return  ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<APIResponse> getCompanyById(Long idCompany) {
        log.info("CompanyController::getCompany by idCompany  {}", idCompany);

        CompanyResponseDTO companyResponseDTO = companyServiceImp.getCompanyById(idCompany);
        APIResponse<CompanyResponseDTO> responseDTO = APIResponse
                .<CompanyResponseDTO>builder()
                .status(SUCCESS)
                .results(companyResponseDTO)
                .build();

        log.info("CompanyController::getCompany by idCompany {} response {}", idCompany,companyResponseDTO);

        return new ResponseEntity<>(responseDTO, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<APIResponse> getAllCompanies() {
        Collection<CompanyResponseDTO> companies = companyServiceImp.getCompanies();
        APIResponse<List<CompanyResponseDTO>> responseDTO = APIResponse
                .<List<CompanyResponseDTO>>builder()
                .status(SUCCESS)
                .results((List<CompanyResponseDTO>) companies)
                .build();

        log.info("CompanyController::getAllCompanies response {}", responseDTO);

        return new ResponseEntity<>(responseDTO, HttpStatus.OK);
    }
}
