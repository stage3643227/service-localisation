package tn.sofrecom.servicelocalisation.controller.site;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tn.sofrecom.servicelocalisation.dto.APIResponse;
import tn.sofrecom.servicelocalisation.dto.site.SiteRequestDTO;

import javax.validation.Valid;

@RestController
public interface SiteController {
    @PostMapping
    ResponseEntity<APIResponse> createNewSite(@Valid @RequestBody SiteRequestDTO siteRequestDTO);
    @PutMapping
    ResponseEntity<APIResponse> updateSite(@Valid @RequestBody SiteRequestDTO siteRequestDTO);
    @DeleteMapping("/{idSite}")
    ResponseEntity<Void> deleteSite(@PathVariable Long idSite);
    @GetMapping("/{idSite}")
    ResponseEntity<APIResponse> getSiteById(@PathVariable Long idSite);
    @GetMapping("/all")
    ResponseEntity<APIResponse> getAllSites();
}
