package tn.sofrecom.servicelocalisation.controller.site.implement;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tn.sofrecom.servicelocalisation.controller.site.SiteController;
import tn.sofrecom.servicelocalisation.dto.APIResponse;
import tn.sofrecom.servicelocalisation.dto.site.SiteMonitorResponseDTO;
import tn.sofrecom.servicelocalisation.dto.site.SiteRequestDTO;
import tn.sofrecom.servicelocalisation.dto.site.SiteResponseDTO;
import tn.sofrecom.servicelocalisation.dto.site.SiteTelecomResponseDTO;
import tn.sofrecom.servicelocalisation.service.site.implement.SiteServiceImp;

import javax.validation.Valid;
import java.util.Collection;
import java.util.List;

@RestController
@Slf4j
@AllArgsConstructor
@RequestMapping("/api/v1/Sites")
@CrossOrigin(origins = "http://localhost:4200")
public class SiteControllerImp implements SiteController {
    public static final String SUCCESS = "Success";
    private SiteServiceImp siteServiceImp;
    @Override
    public ResponseEntity<APIResponse> createNewSite(@Valid @RequestBody SiteRequestDTO siteRequestDTO) {
        log.info("SiteController::createNewSite request body {}", siteRequestDTO);
        SiteResponseDTO siteResponseDTO = siteServiceImp.createNewSite(siteRequestDTO);
        APIResponse<SiteResponseDTO> responseDTO =   APIResponse
                .<SiteResponseDTO>builder()
                .status(SUCCESS)
                .results(siteResponseDTO)
                .build();

        log.info("SiteController::createNewSite {}", siteResponseDTO );
        return new ResponseEntity<>(responseDTO, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<APIResponse> updateSite(SiteRequestDTO siteRequestDTO) {
        log.info("SiteController::updateSite request body {}", siteRequestDTO);
        SiteResponseDTO siteResponseDTO = siteServiceImp.updateSite(siteRequestDTO);
        APIResponse<SiteResponseDTO> responseDTO = APIResponse
                .<SiteResponseDTO>builder()
                .status(SUCCESS)
                .results(siteResponseDTO)
                .build();

        log.info("SiteController::updateSiteresponse {}", siteResponseDTO );
        return new ResponseEntity<>(responseDTO, HttpStatus.ACCEPTED);
    }

    @Override
    public ResponseEntity<Void> deleteSite(Long idSite) {
        log.info("SiteController::deleteSite by idSite  {}", idSite);
        siteServiceImp.deleteSite(idSite);
        return  ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<APIResponse> getSiteById(Long idSite) {
        log.info("SiteController::getSite by idSite  {}", idSite);

        SiteResponseDTO siteResponseDTO = siteServiceImp.getSiteById(idSite);
        APIResponse<SiteResponseDTO> responseDTO = APIResponse
                .<SiteResponseDTO>builder()
                .status(SUCCESS)
                .results(siteResponseDTO)
                .build();

        log.info("SiteController::getSite by idSite {} response {}", idSite,siteResponseDTO);

        return new ResponseEntity<>(responseDTO, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<APIResponse> getAllSites() {
        Collection<SiteResponseDTO> sites = siteServiceImp.getSites();
        APIResponse<List<SiteResponseDTO>> responseDTO = APIResponse
                .<List<SiteResponseDTO>>builder()
                .status(SUCCESS)
                .results((List<SiteResponseDTO>) sites)
                .build();

        log.info("SiteController::getAllSites response {}", responseDTO);

        return new ResponseEntity<>(responseDTO, HttpStatus.OK);
    }

    @GetMapping("getSitesByIdCompany/{idCompany}")
    public ResponseEntity<APIResponse> getSitesByIdCompany(@PathVariable Long idCompany) {
        Collection<SiteResponseDTO> sites = siteServiceImp.getSitesByIdCompany(idCompany);
        APIResponse<List<SiteResponseDTO>> responseDTO = APIResponse
                .<List<SiteResponseDTO>>builder()
                .status(SUCCESS)
                .results((List<SiteResponseDTO>) sites)
                .build();

        log.info("SiteController::getSitesByIdCompany response {}", responseDTO);

        return new ResponseEntity<>(responseDTO, HttpStatus.OK);
    }
    @GetMapping("getSiteMonitorById/{idSite}")
    public ResponseEntity<APIResponse> getSiteMonitorById(@PathVariable Long idSite) {
        log.info("SiteController::getSiteMonitorById by idSite  {}", idSite);

        SiteMonitorResponseDTO siteMonitorResponseDTO = siteServiceImp.getSiteMonitorById(idSite);
        APIResponse<SiteMonitorResponseDTO> responseDTO = APIResponse
                .<SiteMonitorResponseDTO>builder()
                .status(SUCCESS)
                .results(siteMonitorResponseDTO)
                .build();

        log.info("SiteController::getSiteMonitorById by idSite {} response {}", idSite,siteMonitorResponseDTO);

        return new ResponseEntity<>(responseDTO, HttpStatus.OK);
    }
    @PostMapping("getSpecifSite")
    public ResponseEntity<APIResponse> getSpecifSite(@Valid @RequestBody List<Long> sitesId) {
        log.info("SiteController::getSpecifSite request body {}", sitesId);
        List<SiteTelecomResponseDTO> sites = siteServiceImp.getSpecifSite(sitesId);
        APIResponse<List<SiteTelecomResponseDTO>> responseDTO = APIResponse
                .<List<SiteTelecomResponseDTO>>builder()
                .status(SUCCESS)
                .results(sites)
                .build();

        log.info("SiteController::getSpecifSite {}", sites );
        return new ResponseEntity<>(responseDTO, HttpStatus.ACCEPTED);
    }




}
