package tn.sofrecom.servicelocalisation.controller.topology;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tn.sofrecom.servicelocalisation.dto.APIResponse;
import tn.sofrecom.servicelocalisation.dto.company.CompanyRequestDTO;
import tn.sofrecom.servicelocalisation.dto.topology.TopologyRequestDTO;
import tn.sofrecom.servicelocalisation.dto.topology.TopologyResponseDTO;

@RestController
public interface TopologyController {
    @PostMapping
    ResponseEntity<APIResponse> createNewTopology(TopologyRequestDTO topologyRequestDTO);
    @PutMapping
    ResponseEntity<APIResponse> updateTopology(TopologyRequestDTO topologyRequestDTO);
    @DeleteMapping("/{idTopology}")
    ResponseEntity<Void> deleteTopology(@PathVariable Long idTopology);
    @GetMapping("/{idTopology}")
    ResponseEntity<APIResponse> getTopologyById(@PathVariable Long idTopology);
    @GetMapping("/all")
    ResponseEntity<APIResponse> getAllTopologies();
}
