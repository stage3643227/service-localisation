package tn.sofrecom.servicelocalisation.controller.topology.implement;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tn.sofrecom.servicelocalisation.controller.topology.TopologyController;
import tn.sofrecom.servicelocalisation.dto.APIResponse;
import tn.sofrecom.servicelocalisation.dto.topology.TopologyRequestDTO;
import tn.sofrecom.servicelocalisation.dto.topology.TopologyResponseDTO;
import tn.sofrecom.servicelocalisation.service.topology.implement.TopologyServiceImp;

import javax.validation.Valid;
import java.util.Collection;
import java.util.List;

@RestController
@Slf4j
@AllArgsConstructor
@RequestMapping("/api/v1/topologies")
@CrossOrigin(origins = "http://localhost:4200")
public class TopologyControllerImp implements TopologyController {
    public static final String SUCCESS = "Success";
    private TopologyServiceImp topologyServiceImp ;
    @Override
    public ResponseEntity<APIResponse> createNewTopology(@Valid @RequestBody TopologyRequestDTO topologyRequestDTO) {
        log.info("TopologyController::createNewTopology request body {}", topologyRequestDTO);
        TopologyResponseDTO topologyResponseDTO = topologyServiceImp.createNewTopology(topologyRequestDTO);
        APIResponse<TopologyResponseDTO> responseDTO =   APIResponse
                .<TopologyResponseDTO>builder()
                .status(SUCCESS)
                .results(topologyResponseDTO)
                .build();

        log.info("TopologyController::createNewTopologyresponse {}", topologyResponseDTO );
        return new ResponseEntity<>(responseDTO, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<APIResponse> updateTopology(@Valid @RequestBody TopologyRequestDTO topologyRequestDTO) {
        log.info("TopologyController::updateCompany request body {}", topologyRequestDTO);
        TopologyResponseDTO topologyResponseDTO = topologyServiceImp.updateTopology(topologyRequestDTO);
        APIResponse<TopologyResponseDTO> responseDTO = APIResponse
                .<TopologyResponseDTO>builder()
                .status(SUCCESS)
                .results(topologyResponseDTO)
                .build();

        log.info("TopologyController::updateTopology response {}", topologyResponseDTO );
        return new ResponseEntity<>(responseDTO, HttpStatus.ACCEPTED);
    }

    @Override
    public ResponseEntity<Void> deleteTopology(Long idTopology) {
        log.info("TopologyController::deleteTopology by idTopology  {}", idTopology);
        topologyServiceImp.deleteTopology(idTopology);
        return  ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<APIResponse> getTopologyById(Long idTopology) {
        log.info("TopologyController::getTopology by idTopology  {}", idTopology);

        TopologyResponseDTO topologyResponseDTO = topologyServiceImp.getTopologyById(idTopology);
        APIResponse<TopologyResponseDTO> responseDTO = APIResponse
                .<TopologyResponseDTO>builder()
                .status(SUCCESS)
                .results(topologyResponseDTO)
                .build();

        log.info("TopologyController::getTopology by idTopology {} response {}", idTopology,topologyResponseDTO);

        return new ResponseEntity<>(responseDTO, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<APIResponse> getAllTopologies() {
        Collection<TopologyResponseDTO> topologies = topologyServiceImp.getTopologies();
        APIResponse<List<TopologyResponseDTO>> responseDTO = APIResponse
                .<List<TopologyResponseDTO>>builder()
                .status(SUCCESS)
                .results((List<TopologyResponseDTO>) topologies)
                .build();

        log.info("TopologyController::getAllTopologies response {}", responseDTO);

        return new ResponseEntity<>(responseDTO, HttpStatus.OK);
    }
}
