package tn.sofrecom.servicelocalisation.dto.company;

import lombok.*;


import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class CompanyRequestDTO {
    private long idCompany;
    @NotBlank(message = "Company name shouldn't be NULL OR EMPTY")
    private String nameCompany;


}
