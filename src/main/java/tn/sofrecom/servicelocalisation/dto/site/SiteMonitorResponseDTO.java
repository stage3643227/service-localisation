package tn.sofrecom.servicelocalisation.dto.site;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class SiteMonitorResponseDTO {
    private long idTopology;
    private String nameTopology;
    private long idCompany;
    private String nameCompany;
}
