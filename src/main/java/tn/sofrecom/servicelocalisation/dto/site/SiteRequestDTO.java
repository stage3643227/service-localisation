package tn.sofrecom.servicelocalisation.dto.site;

import lombok.*;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class SiteRequestDTO {
    private long idSite;
    @NotNull(message = "Site latitude shouldn't be NULL")
    private double latitude;
    @NotNull(message = "Site longitude shouldn't be NULL")
    private double longitude;
    @NotNull(message = "Topology id shouldn't be NULL")
    private long idTopology;
    private String country;
    private String city;



}
