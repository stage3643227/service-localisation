package tn.sofrecom.servicelocalisation.dto.site;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SiteResponseDTO {
    private long idSite;
    private double latitude;
    private double longitude;

    private String country;

    private String city;

    private String nameTopology;

    private String nameCompany;




}
