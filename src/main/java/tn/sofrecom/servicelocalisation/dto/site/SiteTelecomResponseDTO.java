package tn.sofrecom.servicelocalisation.dto.site;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class SiteTelecomResponseDTO {
    private long idSite;
    private String country;
    private String city;


}
