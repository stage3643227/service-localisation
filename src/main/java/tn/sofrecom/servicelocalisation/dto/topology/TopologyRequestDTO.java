package tn.sofrecom.servicelocalisation.dto.topology;

import lombok.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class TopologyRequestDTO {

    private long idTopology;
    @NotBlank(message = "Topology name shouldn't be NULL OR EMPTY")
    private String nameTopology;
    @NotNull(message = "Company id shouldn't be NULL")
    private Long idCompany;

}
