package tn.sofrecom.servicelocalisation.dto.topology;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import tn.sofrecom.servicelocalisation.model.Site;

import java.util.Collection;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TopologyResponseDTO {
    private long idTopology;
    private String nameTopology;
    private String nameCompany;
    private Collection<Site> sites;
}
