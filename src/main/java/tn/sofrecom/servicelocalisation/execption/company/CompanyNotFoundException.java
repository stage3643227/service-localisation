package tn.sofrecom.servicelocalisation.execption.company;

public class CompanyNotFoundException extends  RuntimeException  {
    public CompanyNotFoundException(String message){
        super(message);
    }
}
