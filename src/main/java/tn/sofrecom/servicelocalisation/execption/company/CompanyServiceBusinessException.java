package tn.sofrecom.servicelocalisation.execption.company;

public class CompanyServiceBusinessException extends RuntimeException {
    public CompanyServiceBusinessException(String message){
        super(message);
    }
}
