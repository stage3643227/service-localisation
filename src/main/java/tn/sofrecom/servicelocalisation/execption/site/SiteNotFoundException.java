package tn.sofrecom.servicelocalisation.execption.site;

public class SiteNotFoundException  extends RuntimeException{
    public SiteNotFoundException(String message){
        super(message);
    }
}
