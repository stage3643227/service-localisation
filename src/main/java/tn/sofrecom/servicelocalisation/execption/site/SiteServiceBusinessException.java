package tn.sofrecom.servicelocalisation.execption.site;

public class SiteServiceBusinessException extends RuntimeException{
    public SiteServiceBusinessException(String message){
        super(message);}
}
