package tn.sofrecom.servicelocalisation.execption.topology;

public class TopologyNotFoundException extends  RuntimeException{
    public TopologyNotFoundException(String message){
        super(message);
    }
}
