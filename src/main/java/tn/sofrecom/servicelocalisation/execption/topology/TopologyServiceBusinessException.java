package tn.sofrecom.servicelocalisation.execption.topology;

public class TopologyServiceBusinessException extends  RuntimeException{
    public TopologyServiceBusinessException(String message){
        super(message);
    }
}
