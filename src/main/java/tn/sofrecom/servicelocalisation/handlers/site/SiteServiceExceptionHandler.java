package tn.sofrecom.servicelocalisation.handlers.site;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import tn.sofrecom.servicelocalisation.dto.APIResponse;
import tn.sofrecom.servicelocalisation.dto.ErrorDTO;
import tn.sofrecom.servicelocalisation.execption.site.SiteNotFoundException;
import tn.sofrecom.servicelocalisation.execption.site.SiteServiceBusinessException;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@RestControllerAdvice
public class SiteServiceExceptionHandler {

    public static final String FAILED = "FAILED";
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public APIResponse<?> handleMethodArgumentException(MethodArgumentNotValidException exception) {
        APIResponse<?> serviceResponse = new APIResponse<>();
        List<ErrorDTO> errors = new ArrayList<>();
        exception.getBindingResult().getFieldErrors()
                .forEach(error -> {
                    ErrorDTO errorDTO = new ErrorDTO(error.getField(), error.getDefaultMessage());
                    errors.add(errorDTO);
                });
        serviceResponse.setStatus(FAILED);
        serviceResponse.setErrors(errors);
        return serviceResponse;
    }

    @ExceptionHandler(SiteServiceBusinessException.class)
    public APIResponse<?> handleServiceException(SiteServiceBusinessException exception) {
        APIResponse<?> serviceResponse = new APIResponse<>();
        serviceResponse.setStatus(FAILED);
        serviceResponse.setErrors(Collections.singletonList(new ErrorDTO("", exception.getMessage())));
        return serviceResponse;
    }

    @ExceptionHandler(SiteNotFoundException.class)
    public APIResponse<?> handleProductNotFoundException(SiteNotFoundException exception) {
        APIResponse<?> serviceResponse = new APIResponse<>();
        serviceResponse.setStatus(FAILED);
        serviceResponse.setErrors(Collections.singletonList(new ErrorDTO("", exception.getMessage())));
        return serviceResponse;
    }
}
