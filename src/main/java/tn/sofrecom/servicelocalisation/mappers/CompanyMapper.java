package tn.sofrecom.servicelocalisation.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import tn.sofrecom.servicelocalisation.dto.company.CompanyRequestDTO;
import tn.sofrecom.servicelocalisation.dto.company.CompanyResponseDTO;
import tn.sofrecom.servicelocalisation.model.Company;

@Mapper(componentModel = "spring")
public interface CompanyMapper  {

    CompanyMapper INSTANCE = Mappers.getMapper(CompanyMapper.class);
    Company companyDtoToCompany(CompanyRequestDTO companyRequestDTO);
    @Mapping(source = "company.topology.nameTopology", target = "nameTopology")
    CompanyResponseDTO companyToCompanyResponseDto(Company company);
    Company companyDtoToCompany(CompanyResponseDTO companyResponseDTO);





}
