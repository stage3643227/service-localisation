package tn.sofrecom.servicelocalisation.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import tn.sofrecom.servicelocalisation.dto.site.SiteMonitorResponseDTO;
import tn.sofrecom.servicelocalisation.dto.site.SiteRequestDTO;
import tn.sofrecom.servicelocalisation.dto.site.SiteResponseDTO;
import tn.sofrecom.servicelocalisation.dto.site.SiteTelecomResponseDTO;
import tn.sofrecom.servicelocalisation.model.Site;


@Mapper(componentModel = "spring")
public interface SiteMapper {

    SiteMapper INSTANCE = Mappers.getMapper(SiteMapper.class);

    Site siteDtoToSite(SiteRequestDTO siteRequestDTO);
    Site siteDtoToSite( SiteResponseDTO siteResponseDTO);
    @Mapping(source = "site.topology.nameTopology", target = "nameTopology")
    @Mapping(source = "site.topology.company.nameCompany", target = "nameCompany")
    SiteResponseDTO siteToSiteResponseDto(Site site);
    @Mapping(source = "site.topology.idTopology", target = "idTopology")
    @Mapping(source = "site.topology.nameTopology", target = "nameTopology")
    @Mapping(source = "site.topology.company.nameCompany", target = "nameCompany")
    @Mapping(source = "site.topology.company.idCompany", target = "idCompany")
    SiteMonitorResponseDTO siteToSiteMonitorResponseDTO(Site site);

    SiteTelecomResponseDTO siteToSiteTelecomResponseDTO(Site site);
}
