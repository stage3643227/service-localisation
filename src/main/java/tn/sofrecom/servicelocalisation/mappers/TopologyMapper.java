package tn.sofrecom.servicelocalisation.mappers;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import tn.sofrecom.servicelocalisation.dto.topology.TopologyRequestDTO;
import tn.sofrecom.servicelocalisation.dto.topology.TopologyResponseDTO;
import tn.sofrecom.servicelocalisation.model.Topology;

@Mapper(componentModel = "spring")
public interface TopologyMapper {
    TopologyMapper INSTANCE = Mappers.getMapper(TopologyMapper.class);

    Topology topologyDtoToTopology(TopologyRequestDTO topologyRequestDTO);
    Topology topologyDtoTotopology( TopologyResponseDTO topologyResponseDTO);
    @Mapping(source = "topology.company.nameCompany", target = "nameCompany")
    TopologyResponseDTO topologyTotopologyResponseDto(Topology topology);


}
