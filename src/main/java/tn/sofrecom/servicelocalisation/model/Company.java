package tn.sofrecom.servicelocalisation.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Entity
@Table(name = "COMPANY")
public class Company implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_company")
    @EqualsAndHashCode.Include
    private long idCompany;
    @Column(name = "name_company")
    private String nameCompany;

    @OneToOne(mappedBy = "company" , cascade = CascadeType.REMOVE)
    @JsonBackReference
    private Topology topology;

}
