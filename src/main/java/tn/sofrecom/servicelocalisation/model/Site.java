package tn.sofrecom.servicelocalisation.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Entity
@Table(name = "SITE")
public class Site implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_site")
    private long idSite;
    @Column(name = "latitude")
    private double latitude;
    @Column(name = "longitude")
    private double longitude;
    @Column(name = "country")
    private String country;
    @Column(name = "city")
    private String city;

    @Transient
    private Long idTopology;

    @ManyToOne
    @JsonManagedReference
    @JoinColumn(name = "id_topology")
    @ToString.Exclude
    private Topology topology;

}
