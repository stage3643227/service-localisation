package tn.sofrecom.servicelocalisation.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Entity
@Table(name = "TOPOLOGY",uniqueConstraints = {
        @UniqueConstraint(columnNames = { "id_company" }, name = "UK_topology_id_company")
})
public class Topology implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_topology")
    @EqualsAndHashCode.Include
    private long idTopology;
    @Column(name = "name_topology")
    private String nameTopology;

    @Transient
    private Long idCompany;
    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "id_company", referencedColumnName = "id_company")
    @JsonManagedReference
    @ToString.Exclude
    private Company company;

    @JsonBackReference
    @OneToMany(mappedBy = "topology", cascade = CascadeType.REMOVE)
    private Collection<Site> sites;
}
