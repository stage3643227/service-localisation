package tn.sofrecom.servicelocalisation.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tn.sofrecom.servicelocalisation.model.Company;

@Repository
public interface CompanyRepository extends JpaRepository<Company,Long> {
}
