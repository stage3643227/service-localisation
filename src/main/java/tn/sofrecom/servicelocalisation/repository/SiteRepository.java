package tn.sofrecom.servicelocalisation.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tn.sofrecom.servicelocalisation.model.Site;

import java.util.List;

@Repository
public interface SiteRepository extends JpaRepository<Site,Long> {

    List<Site> findSitesByTopology_Company_IdCompany(Long idCompany);
}
