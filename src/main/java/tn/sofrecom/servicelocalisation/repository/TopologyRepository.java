package tn.sofrecom.servicelocalisation.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tn.sofrecom.servicelocalisation.model.Topology;

@Repository
public interface TopologyRepository extends JpaRepository<Topology,Long> {
}
