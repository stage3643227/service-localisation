package tn.sofrecom.servicelocalisation.service.company;

import tn.sofrecom.servicelocalisation.dto.company.CompanyRequestDTO;
import tn.sofrecom.servicelocalisation.dto.company.CompanyResponseDTO;

import java.util.Collection;

public interface CompanyService {
    CompanyResponseDTO createNewCompany(CompanyRequestDTO companyRequestDTO) ;
    CompanyResponseDTO updateCompany(CompanyRequestDTO companyRequestDTO);
    void deleteCompany(Long idCompany);
    Collection<CompanyResponseDTO> getCompanies();
    CompanyResponseDTO getCompanyById(Long idCompany);


}
