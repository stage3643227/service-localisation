package tn.sofrecom.servicelocalisation.service.company.implement;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tn.sofrecom.servicelocalisation.dto.company.CompanyRequestDTO;
import tn.sofrecom.servicelocalisation.dto.company.CompanyResponseDTO;
import tn.sofrecom.servicelocalisation.execption.company.CompanyNotFoundException;
import tn.sofrecom.servicelocalisation.execption.company.CompanyServiceBusinessException;
import tn.sofrecom.servicelocalisation.mappers.CompanyMapper;
import tn.sofrecom.servicelocalisation.model.Company;
import tn.sofrecom.servicelocalisation.repository.CompanyRepository;
import tn.sofrecom.servicelocalisation.service.company.CompanyService;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Slf4j
@Transactional
public class CompanyServiceImp implements CompanyService {
    private final CompanyRepository companyRepository;
    private final CompanyMapper companyMapper;
    @Override
    public CompanyResponseDTO createNewCompany(CompanyRequestDTO companyRequestDTO) {
        CompanyResponseDTO companyResponseDTO;
        try {
            log.info("CompanyService:createNewCompany execution started.");
            Company company = companyMapper.companyDtoToCompany(companyRequestDTO);
            log.debug("CompanyService:createNewCompany request parameters {}", company);

            Company companyResults = companyRepository.saveAndFlush(company);
            companyResponseDTO = companyMapper.companyToCompanyResponseDto(companyResults);
            log.debug("CompanyService:createNewCompany received response from Database {}", companyResults);

        } catch (Exception ex) {
            log.error("Exception occurred while persisting Company to database , Exception message {}", ex.getMessage());
            throw new CompanyServiceBusinessException("Exception occurred while create a new Company");
        }
        log.info("CompanyService:createNewCompany execution ended.");
        return companyResponseDTO;
    }

    @Override
    public CompanyResponseDTO updateCompany(CompanyRequestDTO companyRequestDTO) {
        CompanyResponseDTO companyResponseDTO;
        Company company;
        try {
            company = companyMapper.companyDtoToCompany(getCompanyById(companyRequestDTO.getIdCompany()));
            log.info("CompanyService:updateCompany execution started.");
            company.setNameCompany(companyMapper.companyDtoToCompany(companyRequestDTO).getNameCompany());
            companyResponseDTO = companyMapper.companyToCompanyResponseDto(companyRepository.saveAndFlush(company));
        } catch (Exception ex) {
            log.error("Exception occurred while persisting Company to database , Exception message {}", ex.getMessage());
            throw new CompanyServiceBusinessException("Exception occurred while update a Company");
        }
        return companyResponseDTO;
    }

    @Override
    public void deleteCompany(Long idCompany) {
        log.debug("Request to delete company: {}", idCompany);
        if (getCompanyById(idCompany) != null)
            companyRepository.deleteById(idCompany);
        else {
            throw new CompanyServiceBusinessException("Exception occurred while delete a Company");
        }
    }

    @Override
    public Collection<CompanyResponseDTO> getCompanies() {
        Collection<CompanyResponseDTO> companyResponseDTOS = null;
        try {
            log.info("CompanyService:getCompanies execution started.");
            Collection<Company> companyList = companyRepository.findAll();

            if (!companyList.isEmpty()) {
                companyResponseDTOS = companyList.stream().sorted(Comparator.comparing(Company::getIdCompany))
                        .map(companyMapper::companyToCompanyResponseDto)
                        .collect(Collectors.toList());
            } else {
                companyResponseDTOS = Collections.emptyList();
            }
            log.debug("CompanyService:getCompanies retrieving companies from database  {}", companyResponseDTOS);
        } catch (Exception ex) {
            log.error("Exception occurred while retrieving companies from database , Exception message {}", ex.getMessage());
            throw new CompanyServiceBusinessException("Exception occurred while fetch all Companies from Database");
        }
        log.info("CompanyService:getCompanies execution ended.");
        return companyResponseDTOS;


    }

    @Override
    @Transactional(
            readOnly = true
    )
    public CompanyResponseDTO getCompanyById(Long idCompany) {
        CompanyResponseDTO companyResponseDTO;
        try {
            log.info("CompanyService:getCompanyById execution started.");
            Company company = companyRepository.findById(idCompany)
                    .orElseThrow(() -> new CompanyNotFoundException("Company not found with id " + idCompany));
            companyResponseDTO = companyMapper.companyToCompanyResponseDto(company);
            log.debug("CompanyService:getCompanyById retrieving company  from database for id {} {}", idCompany, companyResponseDTO);
        } catch (Exception ex) {
            log.error("Exception occurred while retrieving company {} from database , Exception message {}", idCompany, ex.getMessage());
            throw new CompanyServiceBusinessException("Exception occurred while fetch company from Database " + idCompany);
        }
        log.info("CompanyService:getCompanyById execution ended.");
        return companyResponseDTO;
    }
}
