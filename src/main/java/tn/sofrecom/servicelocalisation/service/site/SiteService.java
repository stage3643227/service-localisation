package tn.sofrecom.servicelocalisation.service.site;

import org.springframework.web.bind.annotation.PathVariable;
import tn.sofrecom.servicelocalisation.dto.site.SiteRequestDTO;
import tn.sofrecom.servicelocalisation.dto.site.SiteResponseDTO;

import java.util.Collection;

public interface SiteService {
    SiteResponseDTO createNewSite(SiteRequestDTO siteRequestDTO) ;
    SiteResponseDTO updateSite(SiteRequestDTO siteRequestDTO);
    void deleteSite(@PathVariable Long idSite);
    Collection<SiteResponseDTO> getSites();
    SiteResponseDTO getSiteById(@PathVariable Long idSite);
}
