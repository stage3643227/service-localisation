package tn.sofrecom.servicelocalisation.service.site.implement;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import tn.sofrecom.servicelocalisation.dto.site.SiteMonitorResponseDTO;
import tn.sofrecom.servicelocalisation.dto.site.SiteRequestDTO;
import tn.sofrecom.servicelocalisation.dto.site.SiteResponseDTO;
import tn.sofrecom.servicelocalisation.dto.site.SiteTelecomResponseDTO;
import tn.sofrecom.servicelocalisation.dto.topology.TopologyResponseDTO;
import tn.sofrecom.servicelocalisation.execption.site.SiteNotFoundException;
import tn.sofrecom.servicelocalisation.execption.site.SiteServiceBusinessException;
import tn.sofrecom.servicelocalisation.execption.topology.TopologyServiceBusinessException;
import tn.sofrecom.servicelocalisation.mappers.SiteMapper;
import tn.sofrecom.servicelocalisation.mappers.TopologyMapper;
import tn.sofrecom.servicelocalisation.model.Site;
import tn.sofrecom.servicelocalisation.repository.SiteRepository;
import tn.sofrecom.servicelocalisation.service.site.SiteService;
import tn.sofrecom.servicelocalisation.service.topology.implement.TopologyServiceImp;

import java.util.*;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Slf4j
@Transactional
public class SiteServiceImp implements SiteService {
    private final TopologyServiceImp topologyServiceImp;
    private final SiteMapper siteMapper;
    private final SiteRepository siteRepository;

    private final TopologyMapper topologyMapper;


    @Override
    public SiteResponseDTO createNewSite(SiteRequestDTO siteRequestDTO) {
        SiteResponseDTO siteResponseDTO;

        try {
            log.info("SiteService:createNewSite execution started.");
            Site site = siteMapper.siteDtoToSite(siteRequestDTO);
            TopologyResponseDTO topologyResponseDTO = topologyServiceImp.getTopologyById(site.getIdTopology());
            site.setTopology(topologyMapper.topologyDtoTotopology(topologyResponseDTO));
            log.debug("SiteService:createNewSite  request parameters {}", site);
            Site siteResults = siteRepository.saveAndFlush(site);
            siteResponseDTO = siteMapper.siteToSiteResponseDto(siteResults);
            log.debug("SiteService:createNewSite received response from Database {}", siteResults);

        } catch (Exception ex) {
            log.error("Exception occurred while persisting Site to database , Exception message {}", ex.getMessage());
            throw new SiteServiceBusinessException("Exception occurred while create a new Site");
        }
        log.info("SiteService:createNewSite execution ended.");
        return siteResponseDTO;
    }

    @Override
    public SiteResponseDTO updateSite(SiteRequestDTO siteRequestDTO) {

        SiteResponseDTO siteResponseDTO;
        TopologyResponseDTO topologyResponseDTO;
        Site site;

        try {
            site = siteMapper.siteDtoToSite(getSiteById(siteRequestDTO.getIdSite()));
            log.info("SiteService:updateSite execution started.");
            site.setCountry(siteRequestDTO.getCountry());
            site.setCity(siteRequestDTO.getCity());
            site.setLongitude(siteRequestDTO.getLongitude());
            site.setLatitude(siteRequestDTO.getLatitude());
            topologyResponseDTO = topologyServiceImp.getTopologyById(siteRequestDTO.getIdTopology());
            site.setTopology(topologyMapper.topologyDtoTotopology(topologyResponseDTO));
            siteResponseDTO = siteMapper.siteToSiteResponseDto(siteRepository.saveAndFlush(site));
        } catch (Exception ex) {
            log.error("Exception occurred while persisting Site to database , Exception message {}", ex.getMessage());
            throw new TopologyServiceBusinessException("Exception occurred while update a Site");
        }
        return siteResponseDTO;
    }

    @Override
    public void deleteSite(Long idSite) {
        log.debug("Request to delete Site: {}", idSite);
        if (getSiteById(idSite) != null)
            siteRepository.deleteById(idSite);
        else {
            throw new SiteServiceBusinessException("Exception occurred while delete a Site");
        }
    }

    @Override
    public Collection<SiteResponseDTO> getSites() {
        Collection<SiteResponseDTO> siteResponseDTOS = null;
        try {
            log.info("SiteService::getSites execution started.");
            Collection<Site> siteyList = siteRepository.findAll();

            if (!siteyList.isEmpty()) {
                siteResponseDTOS = siteyList.stream().sorted(Comparator.comparing(Site::getIdSite))
                        .map(siteMapper::siteToSiteResponseDto)
                        .collect(Collectors.toList());
            } else {
                siteResponseDTOS = Collections.emptyList();
            }
            log.debug("SiteService:getSites retrieving getSites from database  {}", siteResponseDTOS);

        } catch (Exception ex) {
            log.error("Exception occurred while retrieving sites from database , Exception message {}", ex.getMessage());
            throw new TopologyServiceBusinessException("Exception occurred while fetch all sites from Database");
        }

        log.info("SiteService:getSites execution ended.");
        return siteResponseDTOS;
    }

    @Override
    public SiteResponseDTO getSiteById(Long idSite) {
        SiteResponseDTO siteResponseDTO;
        try {
            log.info("SiteService:getSiteById execution started.");
            Site site = siteRepository.findById(idSite)
                    .orElseThrow(() -> new SiteNotFoundException("Site not found with id " + idSite));
            siteResponseDTO = siteMapper.siteToSiteResponseDto(site);
            log.debug("SiteService:getSiteById retrieving Site  from database for idSite {} {}", idSite, siteResponseDTO);

        } catch (Exception ex) {
            log.error("Exception occurred while retrieving Site {} from database , Exception message {}", idSite, ex.getMessage());
            throw new SiteServiceBusinessException("Exception occurred while fetch Site from Database " + idSite);
        }

        log.info("SiteService:getSiteById execution ended.");
        return siteResponseDTO;
    }

    public Collection<SiteResponseDTO> getSitesByIdCompany(@PathVariable Long idCompany) {
        Collection<SiteResponseDTO> siteResponseDTOS = null;
        try {
            log.info("SiteService::getSitesByIdCompany execution started.");
            Collection<Site> siteyList = siteRepository.findSitesByTopology_Company_IdCompany(idCompany);

            if (!siteyList.isEmpty()) {
                siteResponseDTOS = siteyList.stream().sorted(Comparator.comparing(Site::getIdSite))
                        .map(siteMapper::siteToSiteResponseDto)
                        .collect(Collectors.toList());
            } else {
                siteResponseDTOS = Collections.emptyList();
            }
            log.debug("SiteService:getSites retrieving getSitesByIdCompany from database  {}", siteResponseDTOS);

        } catch (Exception ex) {
            log.error("Exception occurred while retrieving Sites from database , Exception message {}", ex.getMessage());
            throw new TopologyServiceBusinessException("Exception occurred while fetch all sites from Database");
        }

        log.info("SiteService:getSites execution ended.");
        return siteResponseDTOS;
    }
    public SiteMonitorResponseDTO getSiteMonitorById(Long idSite) {
        SiteMonitorResponseDTO siteMonitorResponseDTO;
        try {
            log.info("SiteService:getSiteMonitorById execution started.");
            Site site = siteRepository.findById(idSite)
                    .orElseThrow(() -> new SiteNotFoundException("Site not found with id " + idSite));
            siteMonitorResponseDTO = siteMapper.siteToSiteMonitorResponseDTO(site);
            log.debug("SiteService:getSiteMonitorById retrieving Site  from database for idSite {} {}", idSite, siteMonitorResponseDTO);

        } catch (Exception ex) {
            log.error("Exception occurred while retrieving Site {} from database , Exception message {}", idSite, ex.getMessage());
            throw new SiteServiceBusinessException("Exception occurred while fetch Site from Database " + idSite);
        }

        log.info("SiteService:getSiteMonitorById execution ended.");
        return siteMonitorResponseDTO;
    }
    public  List<SiteTelecomResponseDTO> getSpecifSite(List<Long> sitesId) {
        List<SiteTelecomResponseDTO>  siteResponseDTOS = null;
        List<Site> sites = new ArrayList<>();
        try {
            if (sitesId != null && !sitesId.isEmpty()) {

                for (Long id : sitesId) {
                    Site site = siteRepository.findById(id)
                            .orElseThrow(() -> new SiteNotFoundException("Site not found with id " + id));
                    sites.add(site);
                }
            }
            if (!sites.isEmpty()) {
                siteResponseDTOS = sites.stream().sorted(Comparator.comparing(Site::getIdSite))
                        .map(siteMapper::siteToSiteTelecomResponseDTO)
                        .collect(Collectors.toList());
            } else {
                siteResponseDTOS = Collections.emptyList();
            }
        } catch (Exception ex) {
            log.error("Exception occurred while retrieving Site {} from database , Exception message {}", sitesId.toString(), ex.getMessage());
            throw new SiteServiceBusinessException("Exception occurred while fetch Site from Database " + sitesId.toString());
        }

        log.info("SiteService::getSpecifSite execution ended.");
        return siteResponseDTOS;
    }

}
