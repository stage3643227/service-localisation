package tn.sofrecom.servicelocalisation.service.topology;

import org.springframework.web.bind.annotation.PathVariable;
import tn.sofrecom.servicelocalisation.dto.topology.TopologyRequestDTO;
import tn.sofrecom.servicelocalisation.dto.topology.TopologyResponseDTO;

import java.util.Collection;

public interface TopologyService {
    TopologyResponseDTO createNewTopology(TopologyRequestDTO topologyRequestDTO) ;
    TopologyResponseDTO updateTopology(TopologyRequestDTO topologyRequestDTO);
    void deleteTopology(@PathVariable Long idTopology);
    Collection<TopologyResponseDTO> getTopologies();
    TopologyResponseDTO getTopologyById(@PathVariable Long idTopology);
}
