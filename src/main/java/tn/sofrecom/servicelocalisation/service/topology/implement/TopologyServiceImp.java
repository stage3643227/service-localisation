package tn.sofrecom.servicelocalisation.service.topology.implement;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tn.sofrecom.servicelocalisation.dto.company.CompanyResponseDTO;
import tn.sofrecom.servicelocalisation.dto.topology.TopologyRequestDTO;
import tn.sofrecom.servicelocalisation.dto.topology.TopologyResponseDTO;
import tn.sofrecom.servicelocalisation.execption.company.CompanyNotFoundException;
import tn.sofrecom.servicelocalisation.execption.topology.TopologyServiceBusinessException;
import tn.sofrecom.servicelocalisation.mappers.CompanyMapper;
import tn.sofrecom.servicelocalisation.mappers.TopologyMapper;
import tn.sofrecom.servicelocalisation.model.Topology;
import tn.sofrecom.servicelocalisation.repository.TopologyRepository;
import tn.sofrecom.servicelocalisation.service.company.implement.CompanyServiceImp;
import tn.sofrecom.servicelocalisation.service.topology.TopologyService;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Slf4j
@Transactional
public class TopologyServiceImp implements TopologyService {
    private final TopologyRepository topologyRepository ;
    private  final CompanyServiceImp companyServiceImp ;
    private final CompanyMapper companyMapper ;
    private final TopologyMapper topologyMapper ;
    @Override
    public TopologyResponseDTO createNewTopology(TopologyRequestDTO topologyRequestDTO) {
        TopologyResponseDTO topologyResponseDTO;

        try {
            log.info("TopologyService:createNewTopology execution started.");
            Topology topology = topologyMapper.topologyDtoToTopology(topologyRequestDTO);
            log.info("TopologyService:createNewTopology topology {}.",topology);
            CompanyResponseDTO companyResponseDTO = companyServiceImp.getCompanyById(topology.getIdCompany());
            topology.setCompany(companyMapper.companyDtoToCompany(companyResponseDTO));
            log.debug("TopologyService:createNewTopology request parameters {}", topology);
            Topology topologyResults = topologyRepository.saveAndFlush(topology);
            topologyResponseDTO = topologyMapper.topologyTotopologyResponseDto(topologyResults);
            log.debug("TopologyService:createNewTopology received response from Database {}", topologyResults);

        } catch (Exception ex) {
            log.error("Exception occurred while persisting Topology to database , Exception message {}", ex.getMessage());
            throw new TopologyServiceBusinessException("Exception occurred while create a new Topology");
        }
        log.info("TopologyService:createNewTopology execution ended.");
        return topologyResponseDTO;
    }

    @Override
    public TopologyResponseDTO updateTopology(TopologyRequestDTO topologyRequestDTO) {
        TopologyResponseDTO topologyResponseDTO;
        Topology topology ;

        try {
            topology = topologyMapper.topologyDtoTotopology(getTopologyById(topologyRequestDTO.getIdTopology()));
            log.info("TopologyService:updateTopology execution started.");
            topology.setNameTopology(topologyRequestDTO.getNameTopology());
            CompanyResponseDTO companyResponseDTO = companyServiceImp.getCompanyById(topologyRequestDTO.getIdCompany());
            topology.setCompany(companyMapper.companyDtoToCompany(companyResponseDTO));
            topologyResponseDTO = topologyMapper.topologyTotopologyResponseDto(topologyRepository.saveAndFlush(topology));
        } catch (Exception ex) {
            log.error("Exception occurred while persisting Topology to database , Exception message {}", ex.getMessage());
            throw new TopologyServiceBusinessException("Exception occurred while update a Topology");
        }
        return topologyResponseDTO;
    }

    @Override
    public void deleteTopology(Long idTopology) {
        log.debug("Request to delete topology: {}", idTopology);
        if (getTopologyById(idTopology) != null)
            topologyRepository.deleteById(idTopology);
        else {
            throw new TopologyServiceBusinessException("Exception occurred while delete a topology");
        }
    }

    @Override
    public Collection<TopologyResponseDTO> getTopologies() {
        Collection<TopologyResponseDTO> topologyResponseDTOS = null;
        try {
            log.info("TopologyService:getTopologies execution started.");
            Collection<Topology> topologyList = topologyRepository.findAll();

            if (!topologyList.isEmpty()) {
                topologyResponseDTOS = topologyList.stream().sorted(Comparator.comparing(Topology::getIdTopology))
                        .map(topologyMapper::topologyTotopologyResponseDto)
                        .collect(Collectors.toList());
            } else {
                topologyResponseDTOS = Collections.emptyList();
            }
            log.debug("TopologyService:getTopologies retrieving topologies from database  {}", topologyResponseDTOS);

        } catch (Exception ex) {
            log.error("Exception occurred while retrieving topologies from database , Exception message {}", ex.getMessage());
            throw new TopologyServiceBusinessException("Exception occurred while fetch all topologies from Database");
        }

        log.info("CompanyService:getCompanies execution ended.");
        return topologyResponseDTOS;


    }

    @Override
    public TopologyResponseDTO getTopologyById(Long idTopology) {
        TopologyResponseDTO topologyResponseDTO;
        try {
            log.info("TopologyService:getTopologyById execution started.");
            Topology topology = topologyRepository.findById(idTopology)
                    .orElseThrow(() -> new CompanyNotFoundException("Topology not found with id " + idTopology));
            topologyResponseDTO = topologyMapper.topologyTotopologyResponseDto(topology);
            log.debug("TopologyService:getTopologyById retrieving Topology  from database for idTopology {} {}", idTopology, topologyResponseDTO);

        } catch (Exception ex) {
            log.error("Exception occurred while retrieving Topology {} from database , Exception message {}", idTopology, ex.getMessage());
            throw new TopologyServiceBusinessException("Exception occurred while fetch Topology from Database " + idTopology);
        }

        log.info("TopologyService:getTopologyById execution ended.");
        return topologyResponseDTO;
    }

}
