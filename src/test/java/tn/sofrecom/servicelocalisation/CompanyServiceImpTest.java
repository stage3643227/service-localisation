package tn.sofrecom.servicelocalisation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import tn.sofrecom.servicelocalisation.dto.company.CompanyRequestDTO;
import tn.sofrecom.servicelocalisation.dto.company.CompanyResponseDTO;
import tn.sofrecom.servicelocalisation.execption.company.CompanyServiceBusinessException;
import tn.sofrecom.servicelocalisation.mappers.CompanyMapper;
import tn.sofrecom.servicelocalisation.mappers.CompanyMapperImpl;
import tn.sofrecom.servicelocalisation.model.Company;
import tn.sofrecom.servicelocalisation.repository.CompanyRepository;
import tn.sofrecom.servicelocalisation.service.company.implement.CompanyServiceImp;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.any;

@RunWith(MockitoJUnitRunner.class)
class CompanyServiceImpTest {
    @Mock
    private CompanyRepository companyRepository;

    @Mock
    private CompanyMapper companyMapper;

    @InjectMocks
    private CompanyServiceImp companyService;

    private CompanyRequestDTO companyRequestDTO;
    private Company company;
    private Company savedCompany;
    private CompanyResponseDTO expectedCompanyResponseDTO;
    private List<Company> companyList;

    private List<CompanyResponseDTO> companyResponseList ;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        companyRepository = Mockito.mock(CompanyRepository.class);
        companyMapper = Mockito.mock(CompanyMapperImpl.class);
        companyService = new CompanyServiceImp(companyRepository, companyMapper);

        companyRequestDTO = new CompanyRequestDTO();
        companyRequestDTO.setIdCompany(1L);
        companyRequestDTO.setNameCompany("Company 1");

        company = new Company();
        company.setIdCompany(1L);
        company.setNameCompany("Company 1");


        savedCompany = new Company();
        savedCompany.setIdCompany(1L);
        savedCompany.setNameCompany("Company 1");

        expectedCompanyResponseDTO = new CompanyResponseDTO();
        expectedCompanyResponseDTO.setIdCompany(1L);
        expectedCompanyResponseDTO.setNameCompany("Company 1");
        expectedCompanyResponseDTO.setNameTopology(null);


        companyList = new ArrayList<>();
        companyList.add(new Company(1L, "Company A" , null));
        companyList.add(new Company(2L, "Company B", null));
        companyResponseList = new ArrayList<>();
        companyResponseList.add(new CompanyResponseDTO(1L, "Company A" ,null));
        companyResponseList.add(new CompanyResponseDTO(2L, "Company B" , null));






    }

    @Test
    void testCreateNewCompany() {
        when(companyMapper.companyDtoToCompany(companyRequestDTO)).thenReturn(company);
        when(companyRepository.saveAndFlush(company)).thenReturn(savedCompany);
        when(companyMapper.companyToCompanyResponseDto(savedCompany)).thenReturn(expectedCompanyResponseDTO);

        CompanyResponseDTO actualCompanyResponseDTO = companyService.createNewCompany(companyRequestDTO);
        assertNotNull(actualCompanyResponseDTO);
        assertEquals(expectedCompanyResponseDTO.getIdCompany(), actualCompanyResponseDTO.getIdCompany());
        assertEquals(expectedCompanyResponseDTO.getNameCompany(), actualCompanyResponseDTO.getNameCompany());
        assertEquals(expectedCompanyResponseDTO.getNameTopology(), actualCompanyResponseDTO.getNameTopology());
    }


    @Test
     void testUpdateCompany() {

        when(companyRepository.findById(companyRequestDTO.getIdCompany())).thenReturn(Optional.of(company));
        when(companyMapper.companyDtoToCompany(expectedCompanyResponseDTO)).thenReturn(company);
        when(companyMapper.companyDtoToCompany(companyRequestDTO)).thenReturn(company);
        when(companyRepository.saveAndFlush(company)).thenReturn(savedCompany);
        when(companyMapper.companyToCompanyResponseDto(savedCompany)).thenReturn(expectedCompanyResponseDTO);

        CompanyResponseDTO actualCompanyResponseDTO = companyService.updateCompany(companyRequestDTO);
        assertNotNull(actualCompanyResponseDTO);
        assertEquals(expectedCompanyResponseDTO.getIdCompany(), actualCompanyResponseDTO.getIdCompany());
        assertEquals(expectedCompanyResponseDTO.getNameCompany(), actualCompanyResponseDTO.getNameCompany());

    }
    @Test
     void testDeleteCompany() {
        Long idCompany = 1L;
        when(companyRepository.findById(companyRequestDTO.getIdCompany())).thenReturn(Optional.of(company));
        when(companyMapper.companyDtoToCompany(expectedCompanyResponseDTO)).thenReturn(company);
        when(companyMapper.companyToCompanyResponseDto(savedCompany)).thenReturn(expectedCompanyResponseDTO);

        companyService.deleteCompany(idCompany);
        Mockito.verify(companyRepository, Mockito.times(1)).deleteById(idCompany);
    }
    @Test
   void getCompanyByIdWhenValidIdShouldReturnCompanyResponseDto() {
        Long idCompany = 1L;

        when(companyRepository.findById(companyRequestDTO.getIdCompany())).thenReturn(Optional.of(company));
        when(companyMapper.companyDtoToCompany(expectedCompanyResponseDTO)).thenReturn(company);
        when(companyMapper.companyToCompanyResponseDto(savedCompany)).thenReturn(expectedCompanyResponseDTO);

        CompanyResponseDTO result = companyService.getCompanyById(idCompany);
        assertEquals(idCompany, result.getIdCompany());
    }

    @Test
    void getCompanyByIdWhenExceptionThrownShouldThrowException() {
        Long idCompany = 1L;
        when(companyRepository.findById(companyRequestDTO.getIdCompany())).thenReturn(Optional.of(company));
        when(companyMapper.companyDtoToCompany(expectedCompanyResponseDTO)).thenReturn(company);
        when(companyMapper.companyToCompanyResponseDto(savedCompany)).thenReturn(expectedCompanyResponseDTO);

        when(companyRepository.findById(anyLong())).thenThrow(new RuntimeException("Exception occurred while fetch company from Database"));
        assertThrows(CompanyServiceBusinessException.class, () -> {
            companyService.getCompanyById(idCompany);
        });
    }
    @Test
    void testGetCompanies() {

        when(companyRepository.findAll()).thenReturn(companyList);
        when(companyMapper.companyToCompanyResponseDto(any())).thenReturn(companyResponseList.get(0), companyResponseList.get(1));

        Collection<CompanyResponseDTO> result = companyService.getCompanies();

        assertNotNull(result);
        assertEquals(companyList.size(), result.size());

        List<CompanyResponseDTO> resultList = new ArrayList<>(result);
        assertEquals(companyResponseList.get(0), resultList.get(0));
        assertEquals(companyResponseList.get(1), resultList.get(1));
    }

}
