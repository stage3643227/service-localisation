package tn.sofrecom.servicelocalisation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import tn.sofrecom.servicelocalisation.dto.site.SiteRequestDTO;
import tn.sofrecom.servicelocalisation.dto.site.SiteResponseDTO;
import tn.sofrecom.servicelocalisation.dto.topology.TopologyResponseDTO;
import tn.sofrecom.servicelocalisation.execption.site.SiteServiceBusinessException;
import tn.sofrecom.servicelocalisation.mappers.*;
import tn.sofrecom.servicelocalisation.model.Site;
import tn.sofrecom.servicelocalisation.model.Topology;
import tn.sofrecom.servicelocalisation.repository.CompanyRepository;
import tn.sofrecom.servicelocalisation.repository.SiteRepository;
import tn.sofrecom.servicelocalisation.repository.TopologyRepository;
import tn.sofrecom.servicelocalisation.service.company.implement.CompanyServiceImp;
import tn.sofrecom.servicelocalisation.service.site.implement.SiteServiceImp;
import tn.sofrecom.servicelocalisation.service.topology.implement.TopologyServiceImp;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
class SiteServiceImpTest {

    @Mock
    private SiteServiceImp siteService;

    @Mock
    private SiteMapper siteMapper;
    @Mock
    private SiteRepository siteRepository;
    @Mock
    private TopologyRepository topologyRepository;
    @InjectMocks
    private TopologyServiceImp topologyService;
    @Mock
    private TopologyMapper topologyMapper;

    @Mock
    private CompanyMapper companyMapper;
    @Mock
    private CompanyRepository companyRepository;
    @Mock
    private CompanyServiceImp companyService;
    private SiteRequestDTO siteRequestDTO ;
    private SiteResponseDTO siteResponseDTO ;
    private Site site ;
    private Topology topology ;
    private Topology DiffTopology ;

    private TopologyResponseDTO topologyResponseDTO ;
    private TopologyResponseDTO DiffTopologyResponseDTO ;

    private List<Site> siteList;

    private List<SiteResponseDTO> siteResponseList ;

    private List<Site> siteByCompanyList;

    private List<SiteResponseDTO> siteByCompanyResponseList ;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        topologyRepository = Mockito.mock(TopologyRepository.class);
        topologyMapper = Mockito.mock(TopologyMapperImpl.class);
        siteRepository = Mockito.mock(SiteRepository.class);
        siteMapper = Mockito.mock(SiteMapper.class);
        companyMapper = Mockito.mock(CompanyMapperImpl.class);
        companyService = new CompanyServiceImp(companyRepository, companyMapper);
        topologyService= new TopologyServiceImp(topologyRepository,companyService,companyMapper,topologyMapper) ;
        siteService= new SiteServiceImp(topologyService,siteMapper,siteRepository,topologyMapper) ;

        siteRequestDTO = new SiteRequestDTO();
        siteRequestDTO.setIdSite(1L);
        siteRequestDTO.setLongitude(0.1);
        siteRequestDTO.setLatitude(0.8);
        siteRequestDTO.setCity("Sfax");
        siteRequestDTO.setCountry("Tunisie");
        siteRequestDTO.setIdTopology(1L);

        site = new Site();
        site.setIdSite(1L);
        site.setLongitude(0.1);
        site.setLatitude(0.8);
        site.setCity("Sfax");
        site.setCountry("Tunisie");
        site.setIdTopology(1L);
        site.setTopology(topology);

        topology = new Topology();
        topology.setIdTopology(1L);
        topology.setNameTopology("Test Topology");
        topology.setIdCompany(1L);

        DiffTopology = new Topology();
        DiffTopology.setIdTopology(2L);
        DiffTopology.setNameTopology("DIFF Topology");
        DiffTopology.setIdCompany(2L);

        topologyResponseDTO = new TopologyResponseDTO() ;
        topologyResponseDTO.setIdTopology(2L);
        topologyResponseDTO.setNameTopology("Test Topology");
        topologyResponseDTO.setNameCompany("Test Company");

        DiffTopologyResponseDTO = new TopologyResponseDTO() ;
        DiffTopologyResponseDTO.setIdTopology(2L);
        DiffTopologyResponseDTO.setNameTopology("DIFF Topology");
        DiffTopologyResponseDTO.setNameCompany("DIFF Company");

        siteResponseDTO = new SiteResponseDTO();
        siteResponseDTO.setIdSite(1L);
        siteResponseDTO.setNameTopology("Test Topology");
        siteResponseDTO.setNameCompany("Test Company");
        siteResponseDTO.setLongitude(0.1);
        siteResponseDTO.setLatitude(0.8);
        siteResponseDTO.setCity("Sfax");
        siteResponseDTO.setCountry("Tunisie");

        siteList = new ArrayList<>();
        siteList.add(new Site(1L,0.8,0.1,"Tunisie","Sfax",1L,topology));
        siteList.add(new Site(2L,9.0,7.0,"France","Paris",2L,DiffTopology));
        siteResponseList = new ArrayList<>();
        siteResponseList.add(new SiteResponseDTO(1L,0.8,0.1,"Tunisie","Sfax","Test Topology","Test Company"));
        siteResponseList.add(new SiteResponseDTO(2L,9.0,7.0,"France","Paris","DIFF Topology","DIFF Company"));

        siteByCompanyList = new ArrayList<>();
        siteByCompanyList.add(new Site(1L,0.8,0.1,"Tunisie","Sfax",1L,topology));
        siteByCompanyList.add(new Site(3L,1.8,2.3,"Tunisie","Tunis",1L,topology));

        siteByCompanyResponseList = new ArrayList<>();
        siteByCompanyResponseList.add(new SiteResponseDTO(1L,0.8,0.1,"Tunisie","Sfax","Test Topology","Test Company"));
        siteByCompanyResponseList.add(new SiteResponseDTO(3L,1.8,2.3,"Tunisie","Tunis","Test Topology","Test Company"));


    }

    @Test
    void testCreateNewSite() {
        when(siteMapper.siteDtoToSite(siteRequestDTO)).thenReturn(site);
        when(topologyMapper.topologyTotopologyResponseDto(topology)).thenReturn(topologyResponseDTO);
        when(topologyRepository.findById(site.getIdTopology())).thenReturn(Optional.of(topology));
        when(topologyService.getTopologyById(1L)).thenReturn(topologyResponseDTO);
        when(topologyMapper.topologyDtoTotopology(topologyResponseDTO)).thenReturn(topology);
        when(siteRepository.saveAndFlush(site)).thenReturn(site);
        when(siteMapper.siteToSiteResponseDto(site)).thenReturn(siteResponseDTO);
        siteResponseDTO = siteService.createNewSite(siteRequestDTO);
        assertEquals(1L, siteResponseDTO.getIdSite());
        assertEquals("Test Topology", siteResponseDTO.getNameTopology());
        assertEquals("Test Company", siteResponseDTO.getNameCompany());
    }

    @Test
    void testUpdateSite()
    {    when(siteRepository.findById(siteRequestDTO.getIdSite())).thenReturn(Optional.of(site));
         when(siteMapper.siteToSiteResponseDto(site)).thenReturn(siteResponseDTO);
         when(siteService.getSiteById(site.getIdSite())).thenReturn(siteResponseDTO);
         when(topologyRepository.findById(site.getIdTopology())).thenReturn(Optional.of(topology));
         when(topologyMapper.topologyTotopologyResponseDto(topology)).thenReturn(topologyResponseDTO);
         when(topologyService.getTopologyById(site.getIdTopology())).thenReturn(topologyResponseDTO);
         when(siteMapper.siteDtoToSite(siteResponseDTO)).thenReturn(site);
         when(siteRepository.saveAndFlush(site)).thenReturn(site);
         when(siteMapper.siteToSiteResponseDto(site)).thenReturn(siteResponseDTO);
         siteResponseDTO = siteService.updateSite(siteRequestDTO);
         assertEquals(1L, siteResponseDTO.getIdSite());
         assertEquals("Test Topology", siteResponseDTO.getNameTopology());
         assertEquals("Test Company", siteResponseDTO.getNameCompany());

    }

    @Test
    void testDeleteSite() {
        Long idSite = 1L;
        when(siteRepository.findById(siteRequestDTO.getIdSite())).thenReturn(Optional.of(site));
        when(siteMapper.siteDtoToSite(siteResponseDTO)).thenReturn(site);
        when(siteMapper.siteToSiteResponseDto(site)).thenReturn(siteResponseDTO);
        siteService.deleteSite(idSite);
        Mockito.verify(siteRepository, Mockito.times(1)).deleteById(idSite);
    }
    @Test
    void getSiteByIdWhenValidIdShouldReturnSiteResponseDto() {
        Long idSite = 1L;

        when(siteRepository.findById(siteRequestDTO.getIdTopology())).thenReturn(Optional.of(site));
        when(siteMapper.siteDtoToSite(siteResponseDTO)).thenReturn(site);
        when(siteMapper.siteToSiteResponseDto(site)).thenReturn(siteResponseDTO);

        SiteResponseDTO result = siteService.getSiteById(idSite);
        assertEquals(idSite, result.getIdSite());
    }
    @Test
    void getSiteByIdWhenExceptionThrownShouldThrowException() {
        Long idSite = 1L;
        when(siteRepository.findById(siteRequestDTO.getIdTopology())).thenReturn(Optional.of(site));
        when(siteMapper.siteDtoToSite(siteResponseDTO)).thenReturn(site);
        when(siteMapper.siteToSiteResponseDto(site)).thenReturn(siteResponseDTO);

        when(siteRepository.findById(anyLong())).thenThrow(new RuntimeException("Exception occurred while fetch site from Database"));
        assertThrows(SiteServiceBusinessException.class, () -> {
            siteService.getSiteById(idSite);
        });

    }
    @Test
    void testGetSites() {

        when(siteRepository.findAll()).thenReturn(siteList);
        when(siteMapper.siteToSiteResponseDto(any())).thenReturn(siteResponseList.get(0),siteResponseList.get(1));

        Collection<SiteResponseDTO> result = siteService.getSites();

        assertNotNull(result);
        assertEquals(siteList.size(), result.size());

        List<SiteResponseDTO> resultList = new ArrayList<>(result);
        assertEquals(siteResponseList.get(0), resultList.get(0));
        assertEquals(siteResponseList.get(1), resultList.get(1));

    }
    @Test
    void getSitesByIdCompany() {
        Long idCompany=1L;
        when(siteRepository.findSitesByTopology_Company_IdCompany(idCompany)).thenReturn(siteByCompanyList);
        when(siteMapper.siteToSiteResponseDto(any())).thenReturn(siteByCompanyResponseList.get(0),siteByCompanyResponseList.get(1));
        Collection<SiteResponseDTO> result = siteService.getSitesByIdCompany(idCompany);
        assertNotNull(result);
        assertEquals(siteByCompanyList.size(), result.size());
        List<SiteResponseDTO> resultList = new ArrayList<>(result);
        assertEquals(siteByCompanyResponseList.get(0), resultList.get(0));
        assertEquals(siteByCompanyResponseList.get(1), resultList.get(1));
    }
}
