package tn.sofrecom.servicelocalisation;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import tn.sofrecom.servicelocalisation.dto.company.CompanyResponseDTO;
import tn.sofrecom.servicelocalisation.dto.topology.TopologyRequestDTO;
import tn.sofrecom.servicelocalisation.dto.topology.TopologyResponseDTO;
import tn.sofrecom.servicelocalisation.execption.topology.TopologyServiceBusinessException;
import tn.sofrecom.servicelocalisation.mappers.CompanyMapper;
import tn.sofrecom.servicelocalisation.mappers.CompanyMapperImpl;
import tn.sofrecom.servicelocalisation.mappers.TopologyMapper;
import tn.sofrecom.servicelocalisation.mappers.TopologyMapperImpl;
import tn.sofrecom.servicelocalisation.model.Company;
import tn.sofrecom.servicelocalisation.model.Topology;
import tn.sofrecom.servicelocalisation.repository.CompanyRepository;
import tn.sofrecom.servicelocalisation.repository.TopologyRepository;
import tn.sofrecom.servicelocalisation.service.company.implement.CompanyServiceImp;
import tn.sofrecom.servicelocalisation.service.topology.implement.TopologyServiceImp;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
 class TopologyServiceImpTest {
    @Mock
    private TopologyMapper topologyMapper;

    @Mock
    private CompanyServiceImp companyService;

    @Mock
    private CompanyMapper companyMapper;
    @Mock
    private CompanyRepository companyRepository;
    @Mock
    private TopologyRepository topologyRepository;

    @InjectMocks
    private TopologyServiceImp topologyService;
    private TopologyRequestDTO topologyRequestDTO ;
    private   CompanyResponseDTO companyResponseDTO ;
    private   Topology topology ;

    private  Company company ;
    private TopologyResponseDTO topologyResponseDTO ;

    private List<Topology> topologyList;

    private List<TopologyResponseDTO> topologyResponseList ;
    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        topologyRepository = Mockito.mock(TopologyRepository.class);
        companyMapper = Mockito.mock(CompanyMapperImpl.class);
        topologyMapper = Mockito.mock(TopologyMapperImpl.class);
        companyRepository = Mockito.mock(CompanyRepository.class);
        companyService = new CompanyServiceImp(companyRepository, companyMapper);
        topologyService= new TopologyServiceImp(topologyRepository,companyService,companyMapper,topologyMapper) ;

        topologyRequestDTO = new TopologyRequestDTO();
        topologyRequestDTO.setIdTopology(1L);
        topologyRequestDTO.setNameTopology("Test Topology");
        topologyRequestDTO.setIdCompany(1L);

        companyResponseDTO = new CompanyResponseDTO();
        companyResponseDTO.setIdCompany(1L);
        companyResponseDTO.setNameCompany("Test Company");

        company = new Company();
        company.setIdCompany(1L);
        company.setNameCompany("Test Company");


        topology = new Topology();
        topology.setIdTopology(1L);
        topology.setNameTopology("Test Topology");
        topology.setIdCompany(1L);
        topology.setCompany(company);

        topologyResponseDTO = new TopologyResponseDTO() ;
        topologyResponseDTO.setIdTopology(1L);
        topologyResponseDTO.setNameTopology("Test Topology");
        topologyResponseDTO.setNameCompany("Test Company");

        topologyList = new ArrayList<>();
        topologyList.add(new Topology(1L,"Topology A",1L,company,null));
        topologyResponseList = new ArrayList<>();
        topologyResponseList.add(new TopologyResponseDTO(1L,"Topology A","Test Company",null));


    }

    @Test
    void testCreateNewTopology() {

        Mockito.when(topologyMapper.topologyDtoToTopology(topologyRequestDTO)).thenReturn(topology);
        Mockito.when(companyMapper.companyToCompanyResponseDto(company)).thenReturn(companyResponseDTO);
        when(companyRepository.findById(topology.getIdCompany())).thenReturn(Optional.of(company));
        Mockito.when(companyService.getCompanyById(topology.getIdCompany())).thenReturn(companyResponseDTO);
        Mockito.when(companyMapper.companyDtoToCompany(companyResponseDTO)).thenReturn(company);
        Mockito.when(topologyMapper.topologyTotopologyResponseDto(topology)).thenReturn(topologyResponseDTO);
        Mockito.when(topologyRepository.saveAndFlush(topology)).thenReturn(topology);
        TopologyResponseDTO topologyResponseDTO = topologyService.createNewTopology(topologyRequestDTO);
        assertEquals(1L, topologyResponseDTO.getIdTopology());
        assertEquals("Test Topology", topologyResponseDTO.getNameTopology());
        assertEquals("Test Company", topologyResponseDTO.getNameCompany());
    }

    @Test
    void testUpdateTopology() {

        when(topologyRepository.findById(topologyRequestDTO.getIdTopology())).thenReturn(Optional.of(topology));
        when(topologyMapper.topologyTotopologyResponseDto(topology)).thenReturn(topologyResponseDTO);
        when(topologyService.getTopologyById(topologyRequestDTO.getIdTopology())).thenReturn(topologyResponseDTO);
        when(companyRepository.findById(topology.getIdCompany())).thenReturn(Optional.of(company));
        when(companyMapper.companyToCompanyResponseDto(company)).thenReturn(companyResponseDTO);
        when(companyService.getCompanyById(topology.getIdCompany())).thenReturn(companyResponseDTO);
        when(companyMapper.companyDtoToCompany(companyResponseDTO)).thenReturn(company);
        when(topologyMapper.topologyDtoTotopology(topologyResponseDTO)).thenReturn(topology);
        when(topologyRepository.saveAndFlush(topology)).thenReturn(topology);
        when(topologyMapper.topologyTotopologyResponseDto(topology)).thenReturn(topologyResponseDTO);
        when(topologyMapper.topologyDtoToTopology(topologyRequestDTO)).thenReturn(topology);
        topologyResponseDTO = topologyService.updateTopology(topologyRequestDTO);
        assertNotNull(topologyResponseDTO);
        assertEquals(1L, topologyResponseDTO.getIdTopology());
        assertEquals("Test Topology", topologyResponseDTO.getNameTopology());
        assertEquals("Test Company", topologyResponseDTO.getNameCompany());

    }
    @Test
    void testDeleteTopology() {
        Long idTopology = 1L;
        when(topologyRepository.findById(topologyRequestDTO.getIdTopology())).thenReturn(Optional.of(topology));
        when(topologyMapper.topologyDtoTotopology(topologyResponseDTO)).thenReturn(topology);
        when(topologyMapper.topologyTotopologyResponseDto(topology)).thenReturn(topologyResponseDTO);
        topologyService.deleteTopology(idTopology);
        Mockito.verify(topologyRepository, Mockito.times(1)).deleteById(idTopology);
    }

    @Test
    void getTopologyByIdWhenValidIdShouldReturnTopologyResponseDto() {
        Long idTopology = 1L;

        when(topologyRepository.findById(topologyRequestDTO.getIdTopology())).thenReturn(Optional.of(topology));
        when(topologyMapper.topologyDtoTotopology(topologyResponseDTO)).thenReturn(topology);
        when(topologyMapper.topologyTotopologyResponseDto(topology)).thenReturn(topologyResponseDTO);

        TopologyResponseDTO result = topologyService.getTopologyById(idTopology);
        assertEquals(idTopology, result.getIdTopology());
    }

    @Test
    void getTopologyByIdWhenExceptionThrownShouldThrowException() {
        Long idTopology = 1L;
        when(topologyRepository.findById(topologyRequestDTO.getIdTopology())).thenReturn(Optional.of(topology));
        when(topologyMapper.topologyDtoTotopology(topologyResponseDTO)).thenReturn(topology);
        when(topologyMapper.topologyTotopologyResponseDto(topology)).thenReturn(topologyResponseDTO);

        when(topologyRepository.findById(anyLong())).thenThrow(new RuntimeException("Exception occurred while fetch topology from Database"));
        assertThrows(TopologyServiceBusinessException.class, () -> {
            topologyService.getTopologyById(idTopology);
        });
    }

    @Test
    void testGetTopologies() {

        when(topologyRepository.findAll()).thenReturn(topologyList);
        System.out.println(topologyList);
        when(topologyMapper.topologyTotopologyResponseDto(any())).thenReturn(topologyResponseList.get(0));

        Collection<TopologyResponseDTO> result = topologyService.getTopologies();

        assertNotNull(result);
        assertEquals(topologyList.size(), result.size());

        List<TopologyResponseDTO> resultList = new ArrayList<>(result);
        assertEquals(topologyResponseList.get(0), resultList.get(0));

    }



}
